#!/bin/bash

# This file is part of AWS Cloud,
# a framework to perform massively parallel verification using AWS:
# https://gitlab.com/sosy-lab/sv-comp/aws-cloud
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

set -e

echo "Running Test…"
# shellcheck disable=SC2164
npm install
wget --no-clobber https://sosy-public.s3.eu-central-1.amazonaws.com/cpachecker-portfolio.zip -O cpachecker-portfolio.zip || :
wget --no-clobber https://sosy-public.s3.eu-central-1.amazonaws.com/tasks.zip || :
wget --no-clobber https://sosy-public.s3.eu-central-1.amazonaws.com/commands-portfolio.json -O commands-portfolio.json || :
node portfolio-test.js

