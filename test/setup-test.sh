#!/bin/bash

# This file is part of AWS Cloud,
# a framework to perform massively parallel verification using AWS:
# https://gitlab.com/sosy-lab/sv-comp/aws-cloud
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

set -e

while [[ $# -gt 0 ]] ; do
  key="$1"
  case $key in
    --portfolio)
        echo "Running Portfolio Test…"
        # shellcheck disable=SC2164
        cd test
        npm install
        wget --no-clobber https://sosy-public.s3.eu-central-1.amazonaws.com/cpachecker-portfolio.zip -O cpachecker-portfolio.zip || :
        wget --no-clobber https://sosy-public.s3.eu-central-1.amazonaws.com/tasks.zip || :
        wget --no-clobber https://sosy-public.s3.eu-central-1.amazonaws.com/commands-portfolio.json -O commands-portfolio.json || :
        node portfolio-test.js
        cd ..
      shift
      ;;
    --hello-world)
        echo "Running Hello World…"
        # shellcheck disable=SC2164
        cd test
        rm -f logfile.txt
        npm install
        wget --no-clobber https://sosy-public.s3.eu-central-1.amazonaws.com/mpi-hello-world.zip -O mpi-hello-world.zip || :
        node multi-node-hello-world-test.js
        cat logfile.txt
        cd ..
      shift
      ;;
    --multi-node)
        echo "Running multi-node portfolio cpachecker…"
        # shellcheck disable=SC2164
        cd test
        npm install
        wget --no-clobber https://sosy-public.s3.eu-central-1.amazonaws.com/cpachecker-portfolio.zip -O cpachecker-portfolio.zip || :
        wget --no-clobber https://sosy-public.s3.eu-central-1.amazonaws.com/tasks.zip || :
        wget --no-clobber https://sosy-public.s3.eu-central-1.amazonaws.com/commands-portfolio.json -O commands-portfolio.json || :
        node portfolio-test.js
        cd ..
      shift
      ;;
    --integration-predicate-overflow)
        echo "Running integration-predicate-overflow cpachecker…"
        # shellcheck disable=SC2164
        cd test
        npm install
        wget --no-clobber https://sosy-public.s3.eu-central-1.amazonaws.com/integration-predicate-overflow-verifier.zip -O integration-predicate-overflow-verifier.zip || :
        wget --no-clobber https://sosy-public.s3.eu-central-1.amazonaws.com/integration-predicate-overflow-tasks.zip -O integration-predicate-overflow-tasks.zip || :
        wget --no-clobber https://sosy-public.s3.eu-central-1.amazonaws.com/commands-integration-predicate-overflow-50.json -O commands-integration-predicate-overflow-50.json || :
        node integration-predicate-overflow-test.js
        cd ..
      shift
      ;;
    --predicate)
        echo "Running predicate test cpachecker…"
        # shellcheck disable=SC2164
        cd test
        npm install
        wget --no-clobber https://sosy-public.s3.eu-central-1.amazonaws.com/predicate-verifier.zip -O predicate-verifier.zip || :
        wget --no-clobber https://sosy-public.s3.eu-central-1.amazonaws.com/predicate-tasks.zip -O predicate-tasks.zip || :
        wget --no-clobber https://sosy-public.s3.eu-central-1.amazonaws.com/commands-predicate-1530.json -O commands-predicate-1530.json || :
        node predicate-test.js
        cd ..
      shift
      ;;
    --clean)
        echo "Removing assets…"
        # shellcheck disable=SC2164
        cd test
        rm -f commands.json commands-portfolio.json logfile.txt ./*.zip commands*.json
        cd ..
      shift
      ;;
    *)
      echo "Invalid param received: $key. Aborting script." >&2
      exit 1
      ;;
  esac
  shift
done

