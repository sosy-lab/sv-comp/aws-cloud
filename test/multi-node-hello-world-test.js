// This file is part of AWS Cloud,
// a framework to perform massively parallel verification using AWS:
// https://gitlab.com/sosy-lab/sv-comp/aws-cloud
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

/*
 * This script tests multi-node execution on Batch.
 *
 * Expects the following files to be present:
 * - mpi-hello-world.zip from https://sosy-public.s3.eu-central-1.amazonaws.com/mpi-hello-world.zip
 */
'use strict';

let helloWorld = "mpi-hello-world.zip";

async function main() {
    const axios = require('axios');

    // 1. Read Configuration
    let fs = require('fs');
    let config = JSON.parse(fs.readFileSync("../all_users.client.config", 'utf8'))[0];

    // 2. Create Job
    let createUrl = config['Endpoint'] + config['UserToken'] + '/execution/create';
    console.log("Calling: " + createUrl);
    let createResponse = await axios.get(
        createUrl,
        {
            responseType: "json"
        });
    let requestId = createResponse['data']['requestId'];
    console.log("Request-Id: " + requestId);

    // 3. Upload verifier & tasks
    let uploadVerifierUrl = config['Endpoint'] + config['UserToken'] + '/upload/' + requestId + '?file=mpi-hello-world.zip';
    console.log("Calling: " + uploadVerifierUrl);
    let uploadVerifierResponse = await axios.get(
        uploadVerifierUrl,
        {
            responseType: "json"
        });
    console.log("Presigned S3 Upload URL: " + uploadVerifierResponse['data']['uploadUrl']);
    await uploadFilePresignedUrl(helloWorld, uploadVerifierResponse['data']['uploadUrl']);

    // 4. Launch computation
    let launchUrl = config['Endpoint'] + config['UserToken'] + '/execution/' + requestId + '/launchBatch';
    console.log("Calling: " + launchUrl);
    console.log("TasksUrl:\t" + " --- ");
    console.log("TasksS3:\t" + " --- ");
    console.log("VerifierUrl (Hello-World):\t" + uploadVerifierResponse['data']['publicURL']);
    console.log("VerifierS3 (Hello-World):\t" + uploadVerifierResponse['data']['S3Key']);
    console.log("commands:\t" + " --- ");

    let launchResponse = await axios.get(
        launchUrl,
        {
            params: {
                verifier: uploadVerifierResponse['data']['publicURL'],
                verifierS3: uploadVerifierResponse['data']['S3Key'],
                commandsS3: " --- "
            },
            responseType: "json",
        });
    console.log("Submitted " + launchResponse['data']['batch'].length + " jobs.");

    // 5. Wait for computation to finish
    let progressUrl = config['Endpoint'] + config['UserToken'] + '/execution/' + requestId + '/progressBatch';
    console.log("Calling: " + progressUrl);
    console.log("Waiting for computation to finish… This might take a while…");
    let completed = null;
    let total;
    let progressResponse = null;
    do {
        await sleep(5000);
        progressResponse = await axios.get(
            progressUrl,
            {
                responseType: "json"
            });
        process.stdout.write(".");
        total = progressResponse['data']['totalNumberOfJobs'];
        if (completed !== progressResponse['data']['totalNumberOfJobsCompleted']) {
            completed = progressResponse['data']['totalNumberOfJobsCompleted'];
            console.log("Completed: " + completed + " of " + total);
        }
    } while (progressResponse['data']['completed'] === false);
    console.log("");

    // 6. Download results
    let resultsUrl = config['Endpoint'] + config['UserToken'] + '/execution/' + requestId + '/results';
    console.log("Calling: " + resultsUrl);
    let resultsResponse = await axios.get(
            resultsUrl,
            {
                responseType: "json"
            });

    console.log("Found " + resultsResponse['data']['urls'].length + " result zip files.");
    for (let i = 0; i < resultsResponse['data']['urls'].length; i++) {
        console.log(resultsResponse['data']['urls'][i]);
        await downloadFile(resultsResponse['data']['urls'][i]);
    }

    console.log("Done!");
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function uploadFilePresignedUrl(pathToFile, presignedUrl) {
    const exec = require('await-exec');
    await exec("curl -X PUT -T " + pathToFile + " '" + presignedUrl + "'");
    //let fs = require('fs');
    //let request = require('request-promise');
    //let options = {
    //    'method': 'PUT',
    //    'url': presignedUrl,
    //    'headers': {
    //        'Content-Type': 'text/plain'
    //    },
    //    body: fs.readFileSync(pathToFile, 'utf8')
    //};
    //await request(options);
    console.log("File " + pathToFile + " was uploaded.");
}

async function downloadFile (url) {
    const Progress = require('progress');
    const urlM = require("url");
    const path = require("path");
    const fs = require("fs");
    const Axios = require('axios');

    const {data, headers} = await Axios({
        url,
        method: 'GET',
        responseType: 'stream'
    });
    const totalLength = headers['content-length'];

    const progressBar = new Progress('-> downloading [:bar] :percent :etas', {
        width: 40,
        complete: '=',
        incomplete: ' ',
        renderThrottle: 1,
        total: parseInt(totalLength)
    });

    let parsed = urlM.parse(url);
    let filename = path.basename(parsed.pathname);

    const writer = fs.createWriteStream(
        path.resolve(__dirname, filename)
    );

    data.on('data', (chunk) => progressBar.tick(chunk.length));
    data.pipe(writer);
}

main();