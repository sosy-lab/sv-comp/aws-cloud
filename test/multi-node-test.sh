#!/bin/bash

# This file is part of AWS Cloud,
# a framework to perform massively parallel verification using AWS:
# https://gitlab.com/sosy-lab/sv-comp/aws-cloud
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

set -e

echo "Running Test…"
# shellcheck disable=SC2164
npm install
wget --no-clobber https://sosy-public.s3.eu-central-1.amazonaws.com/mpi-hello-world.zip -O mpi-hello-world.zip || :
node multi-node-hello-world-test.js
cat logfile.txt

