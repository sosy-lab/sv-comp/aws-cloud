#!/usr/bin/env bash

# This file is part of AWS Cloud,
# a framework to perform massively parallel verification using AWS:
# https://gitlab.com/sosy-lab/sv-comp/aws-cloud
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

set -e

while [[ $# -gt 0 ]] ; do
  key="$1"
  case $key in
    --download)
        echo "Downloading…"
        wget --no-clobber https://sosy-public.s3.eu-central-1.amazonaws.com/predicate-verifier.zip -O predicate-verifier.zip || :
        wget --no-clobber https://sosy-public.s3.eu-central-1.amazonaws.com/predicate-tasks.zip -O predicate-tasks.zip || :
        wget --no-clobber https://sosy-public.s3.eu-central-1.amazonaws.com/commands-predicate-1530.json -O commands-predicate-1530.json || :
      ;;
    --clean)
        echo "Removing assets…"
        rm -f ./*.zip commands*.json
      ;;
    *)
      echo "Invalid param received: $key. Aborting script." >&2
      exit 1
      ;;
  esac
  shift
done

