#!/usr/bin/env bash

# This file is part of AWS Cloud,
# a framework to perform massively parallel verification using AWS:
# https://gitlab.com/sosy-lab/sv-comp/aws-cloud
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# TODO: remove .serverless folder after update?

set -e
#set -x

# TODO: --update ?

source functions.sh

# CTRL+C hook
cleanup () {
  echo "Process aborted."
  exit 1
}
trap 'cleanup' HUP INT TERM

check_dependencies

CONFIGFILE="config/config.json"
REGION="eu-west-1"
PROFILE="default"
DELETE=false
FORCEDELETE=false
CONFIGFILEPARAMETER=false
REGIONPARAMETER=false
PROFILEPARAMETER=false

while [[ $# -gt 0 ]] ; do
  key="$1"
  case $key in
    --help)
      print_help
      exit 0
      ;;
    -p|--profile)
      PROFILE="$2"
      PROFILEPARAMETER=true
      shift
      ;;
    -c|--config)
      CONFIGFILE="$2"
      CONFIGFILEPARAMETER=true
      shift
      ;;
    -r|--region)
      REGION="$2"
      REGIONPARAMETER=true
      shift
      ;;
    -d|--delete)
      DELETE=true
      ;;
    --forcedelete)
      FORCEDELETE=true
      ;;
    test)
      TESTCASE="$2"
      TEST=true
      shift
      ;;
    *)
      echo "Invalid param received: $key. Aborting script." >&2
      exit 1
      ;;
  esac
  shift
done

if [[ "$TEST" = true ]] ; then
    bash test/setup-test.sh "$TESTCASE"
    exit
fi

if [[ ( -f "$CONFIGFILE" || "$CONFIGFILEPARAMETER" = true ) && ("$REGIONPARAMETER" = true || "$PROFILEPARAMETER" = true) ]] ; then
    echo "You specified an config file and a region/profile." \
    "The script will use the values from the config file and ignore the region/profile parameter."
    while true; do
        read -p "Is this really what you want? [yn]: " yn
        case $yn in
            [Yy]* ) break;;
            [Nn]* ) exit; break;;
            * ) echo "Please answer with [y]es or [n]o.";;
        esac
    done
fi

if [[ -f "$CONFIGFILE" ]]; then
    echo "Using existing config file at ${CONFIGFILE}"
    UPDATE=true

    STACKNAME=$(jq -r '.Stack' "$CONFIGFILE")
    HTTPEndpoint=$(jq -r '.HTTPEndpoint' "$CONFIGFILE")
    BUCKETNAME=$(jq -r '.MainBucketName' "$CONFIGFILE")
    INTERNALSECRET=$(jq -r '.InternalSecret' "$CONFIGFILE")
    REGION=$(jq -r '.Region' "$CONFIGFILE")
    PROFILE=$(jq -r '.Profile' "$CONFIGFILE")
    IAMROLEECS=$(jq -r '.ECSIAMRole' "$CONFIGFILE")
    ECSINSTANCEROLE=$(jq -r '.ECSInstanceRole' "$CONFIGFILE")
    INSTANCETYPE=$(jq -r '.InstanceType' "$CONFIGFILE")
    MAXVCPUS=$(jq -r '.MaxVCPUS' "$CONFIGFILE")
    NUMNODES=$(jq -r '.NumNodes' "$CONFIGFILE")
    MINMEMORY=$(jq -r '.MinMemory' "$CONFIGFILE")
    VCPUSJOB=$(jq -r '.VCPUS' "$CONFIGFILE")

    COMPUTEENVIRONMENTNAME=$(jq -r '.BatchComputeEnvironment' "$CONFIGFILE")
    JOBQUEUENAME=$(jq -r '.BatchQueue' "$CONFIGFILE")
    JOBDEFINITIONNAME=$(jq -r '.BatchJobDef' "$CONFIGFILE")

    CONTAINERIMAGE=$(jq -r '.ContainerImage' "$CONFIGFILE")

    TOKEN=$(jq -r '.[0].UserToken' all_users.client.config)

else
    echo "Creating new configuration"
    R=$(head /dev/urandom | tr -dc a-z0-9 | head -c 13)
    UPDATE=false

    STACKNAME="sv-comp-20-aws-${R}"
    DEBUGENDPOINT=$(serverless --aws-profile "$PROFILE" --region "$REGION" deploy | tee /dev/tty | grep "/dev/{user}/execution/create" | cut -d " " -f 5)
    HTTPEndpoint=${DEBUGENDPOINT::-23}
    BUCKETNAME="sv-comp-20-aws-${R}"
    INTERNALSECRET="secret-$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 40 | head -n 1)"
    IAMROLEECS="sv-comp-20-ECSRole${R}"
    ECSINSTANCEROLE="ecsInstanceRole${R}"
    INSTANCETYPE="c5.2xlarge"
    MAXVCPUS="256"
    NUMNODES="4"
    MINMEMORY="2048"
    VCPUSJOB=1

    COMPUTEENVIRONMENTNAME="sv-comp-20-comp-env-${R}"
    JOBQUEUENAME="sv-comp-20-batch-queue${R}"
    JOBDEFINITIONNAME="sv-comp-20-job-def${R}"

    CONTAINERIMAGE="registry.gitlab.com/sosy-lab/sv-comp/aws-cloud"

    TOKEN=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 12 | head -n 1)
fi

echo "Parameters: "
echo " - PROFILE=${PROFILE}"
echo " - CONFIGFILE=${CONFIGFILE}"
echo " - REGION=${REGION}"
echo " - DELETE=${DELETE}"
echo " - FORCEDELETE=${FORCEDELETE}"

if [[ "$DELETE" = true || "$FORCEDELETE" == true ]] ; then
    echo "Removing resources from AWS account (config file: ${CONFIGFILE})"
    STACKNAME=$(jq -r '.Stack' "$CONFIGFILE")
    REGION=$(jq -r '.Region' "$CONFIGFILE")
    PROFILE=$(jq -r '.Profile' "$CONFIGFILE")

    echo "Stack: ${STACKNAME}"
    echo "Region: ${REGION}"
    echo "Profile: ${PROFILE}"

    if [[ "$FORCEDELETE" == false ]] ; then
        while true; do
            read -p "Is this really what you want? [yn]: " yn
            case $yn in
                [Yy]* ) remove_resources; exit;;
                [Nn]* ) exit; break;;
                * ) echo "Please answer with [y]es or [n]o.";;
            esac
        done
    else
        remove_resources
        exit
    fi
fi

write_config

prepare_deploy

deploy

upload_sleep_script

show_config

ask_copy_config








