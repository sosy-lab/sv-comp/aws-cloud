# SPDX-FileCopyrightText: 2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# SPDX-License-Identifier: MIT-0

# The code was taken from:
# https://github.com/aws-samples/aws-mnpbatch-template

# Tutorials with a more detailed explanation can be found at the following to locations (visited on June 2020):
# - https://aws.amazon.com/de/blogs/compute/leveraging-efa-to-run-hpc-and-ml-workloads-on-aws-batch/
# - https://aws.amazon.com/de/blogs/compute/building-a-tightly-coupled-molecular-dynamics-workflow-with-multi-node-parallel-jobs-in-aws-batch/

#!/bin/bash

echo ""
echo "################################"
echo "### Starting MPI Run Script ###"
echo "################################"

BASENAME="${0##*/}"
log () {
  echo "${BASENAME} - ${1}"
}

if [ -z "${HOST_FILE_PATH}" ]; then
  HOST_FILE_PATH={HOME}/hostfile
  log "Could not find env-variable 'HOST_FILE_PATH'. Setting value to '${HOST_FILE_PATH}'."
  export $HOST_FILE_PATH
fi

AWS_BATCH_EXIT_CODE_FILE="/tmp/batch-exit-code"

usage () {
  if [ "${#@}" -ne 0 ]; then
    log "* ${*}"
    log
  fi
  cat <<ENDUSAGE
Usage:
export AWS_BATCH_JOB_NODE_INDEX=0
export AWS_BATCH_JOB_NUM_NODES=10
export AWS_BATCH_JOB_MAIN_NODE_INDEX=0
export AWS_BATCH_JOB_ID=string
./mpi-run.sh
ENDUSAGE

  error_exit
}


# Standard function to print an error and exit with a failing return code
error_exit () {
  log "${BASENAME} - ${1}" >&2
  log "${2:-1}" > $AWS_BATCH_EXIT_CODE_FILE
  kill  $(cat /tmp/supervisord.pid)
}

# Check that the required environment variables are set
if [ -z "${AWS_BATCH_JOB_NODE_INDEX}" ]; then
  usage "AWS_BATCH_JOB_NODE_INDEX not set, unable to determine rank"
fi

if [ -z "${AWS_BATCH_JOB_NUM_NODES}" ]; then
  usage "AWS_BATCH_JOB_NUM_NODES not set. Don't know how many nodes in this job."
fi

if [ -z "${AWS_BATCH_JOB_MAIN_NODE_INDEX}" ]; then
  usage "AWS_BATCH_MULTI_MAIN_NODE_RANK must be set to determine the master node rank"
fi

if [ -z "${MPI_NP}" ]; then
  # TODO: this env variable is not used and will be replaced in a future update
  MPI_NP=${AWS_BATCH_JOB_NUM_NODES}
  log "Env-variable 'MPI_NP' was not set. Using a default value of '${MPI_NP}' for the number of MPI processes"
fi

NODE_TYPE="child"
if [ "${AWS_BATCH_JOB_MAIN_NODE_INDEX}" = "${AWS_BATCH_JOB_NODE_INDEX}" ] ; then
  log "Running job as main node"
  NODE_TYPE="main"
fi


# Main-node: wait for all child-nodes to report their ip-addresses
wait_for_nodes () {
  log "Running as master node"

  touch ${HOST_FILE_PATH}
  chmod a+w ${HOST_FILE_PATH}
  ip=$(/sbin/ip -o -4 addr list eth0 | awk '{print $4}' | cut -d/ -f1)
  availablecores=$(nproc)
  log "master details -> $ip:$availablecores"
  echo "$ip slots=$availablecores" >> $HOST_FILE_PATH

  lines=$(uniq $HOST_FILE_PATH|wc -l)
  while [ "$AWS_BATCH_JOB_NUM_NODES" -gt "$lines" ]
  do
    log "$lines out of $AWS_BATCH_JOB_NUM_NODES nodes joined, will check again in 1 second"
    sleep 1
    lines=$(uniq $HOST_FILE_PATH|wc -l)
  done
  log "All nodes successfully joined"

  # Recreate the hostfile in which potential duplicates are removed.
  mv $HOST_FILE_PATH ${HOST_FILE_PATH}_old
  awk '!a[$0]++' ${HOST_FILE_PATH}_old > ${HOST_FILE_PATH}
  log "Input of hostfile:"
  cat ${HOST_FILE_PATH}

  # Write the hashes of the ip-addresses into known-hosts file
  TEMP_KNOWN_HOSTS_FILE="/tmp/known_hosts.tmp"
  touch $TEMP_KNOWN_HOSTS_FILE
  cat ${HOST_FILE_PATH} | while read line ; do echo $line | cut -d " " -f 1 | xargs -n1 ssh-keyscan -t ssh-ed25519 >> ${TEMP_KNOWN_HOSTS_FILE} ; done
  awk '!a[$0]++' $TEMP_KNOWN_HOSTS_FILE  >> ~/.ssh/known_hosts


  # At this point the program is ready to be run using mpiexec. However, this is not done here as the actual executed command line may differ.
  return 0
}


# Fetch and run a script
report_to_master () {
  # looking for masters nodes ip address calling the batch API
  # get own ip and num cpus
  #
  ip=$(/sbin/ip -o -4 addr list eth0 | awk '{print $4}' | cut -d/ -f1)
  availablecores=$(nproc)
  log "I am a child node -> $ip:$availablecores, reporting to the master node -> ${AWS_BATCH_JOB_MAIN_NODE_PRIVATE_IPV4_ADDRESS}"
  until echo "$ip slots=$availablecores" | ssh -o StrictHostKeyChecking=accept-new "${USER}@${AWS_BATCH_JOB_MAIN_NODE_PRIVATE_IPV4_ADDRESS}" "cat >> ${HOST_FILE_PATH}"
  do
    # All child-nodes are terminated by AWS Batch automatically should the main node exits abruptly.
    log "Failed to pass the ip to the master. Sleeping 3 seconds and trying again"
    sleep 3
  done
  log "Done! Waiting for the main-node to finish its workload."

  # Wait for the main node to create a file containing an exit code. Only then the remaining child-nodes may shut themselves down.
  # This is to prevent the child-nodes shutting themselves down immediately afterwards, as they would then be no longer available to the master-node, resulting in an error on the main-node when it tries to access an already terminated child-node (via mpi, for example).
  while ssh "$USER@${AWS_BATCH_JOB_MAIN_NODE_PRIVATE_IPV4_ADDRESS}" [[ ! -f "${AWS_BATCH_EXIT_CODE_FILE}" ]]; do
      sleep 3
  done

  log "Main-node has finished its task. Shutting down supervisord on this node. Goodbye."
  echo "0" > $AWS_BATCH_EXIT_CODE_FILE
  kill  $(cat /tmp/supervisord.pid)
  return 0
}


# Main - dispatch user request to the appropriate function
case $NODE_TYPE in
  main)
    wait_for_nodes "${@}"
    ;;

  child)
    report_to_master "${@}"
    ;;

  *)
    log $NODE_TYPE
    usage "Could not determine node type. Expected (main/child)"
    ;;
esac
