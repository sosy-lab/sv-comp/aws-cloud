# SPDX-FileCopyrightText: 2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# SPDX-License-Identifier: MIT-0

# The code was taken and adapted from:
# https://github.com/aws-samples/aws-mnpbatch-template

# Tutorials with a more detailed explanation can be found at the following to locations (visited on June 2020):
# - https://aws.amazon.com/de/blogs/compute/leveraging-efa-to-run-hpc-and-ml-workloads-on-aws-batch/
# - https://aws.amazon.com/de/blogs/compute/building-a-tightly-coupled-molecular-dynamics-workflow-with-multi-node-parallel-jobs-in-aws-batch/

#!/bin/bash

# Launch supervisor
BASENAME="${0##*/}"
log () {
  echo "${BASENAME} - ${1}"
}

AWS_BATCH_EXIT_CODE_FILE="/tmp/batch-exit-code"
supervisord -c "/etc/supervisor/supervisord.conf"
sleep 2

# if the supervisor dies then return the exit code stored in ${AWS_BATCH_EXIT_CODE_FILE}
# (instead of taking supervisors exit code)
if [ ! -f $AWS_BATCH_EXIT_CODE_FILE ]; then
    log "Exit code file not found , returning with exit code 1!" >&2
    exit 1
fi

EXIT_CODE=$(cat ${AWS_BATCH_EXIT_CODE_FILE})
log "Batch script ('${AWS_BATCH_EXIT_CODE_FILE}') exited with code ${EXIT_CODE}"
exit $EXIT_CODE
