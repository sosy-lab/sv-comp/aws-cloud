#!/bin/bash

# This file is part of AWS Cloud,
# a framework to perform massively parallel verification using AWS:
# https://gitlab.com/sosy-lab/sv-comp/aws-cloud
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

set -x

echo "Setting up runexec..."

setup_benchexec () {
  for cgroups in $(cut -d : -f 3 /proc/self/cgroup | sort -u); do
    for path in /sys/fs/cgroup/*/"$cgroups"; do
      echo Running "$@" "$path"
      "$@" "$path"
    done
  done
}
setup_benchexec chgrp ${USER}
setup_benchexec chmod g+w
lxcfs /var/lib/lxcfs &

cd /home/${USER}/

if [ -n "$S3_BASH_SCRIPT" ]
then
      echo "Downloading script"
      aws s3 cp "$S3_BASH_SCRIPT" job.sh
      chmod +x job.sh
else 
      echo "Script not defined. Define S3_BASH_SCRIPT"
      exit
fi

if [ -n "$BASH_SCRIPT_FUNCTION" ]
then
      echo "RUNNING FUNCTION"
      echo "$BASH_SCRIPT_FUNCTION"
      source job.sh
      "$BASH_SCRIPT_FUNCTION"
else
      echo "RUNNING SCRIPT"
      bash job.sh
fi

echo "Done"
