#!/usr/bin/env bash

# This file is part of AWS Cloud,
# a framework to perform massively parallel verification using AWS:
# https://gitlab.com/sosy-lab/sv-comp/aws-cloud
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

copy_config () {
    mkdir -p ~/.config/sv-comp-aws
    cp all_users.client.config ~/.config/sv-comp-aws/aws.client.config
}
ask_copy_config () {
    echo ""
    echo "Copy the generated config file to your home dir for repeated use?"
    echo "('~/.config/sv-comp-aws/aws.client.config')"
    while true; do
        read -p "Copy config file [yn]: " yn
        case $yn in
            [Yy]* ) copy_config; break;;
            [Nn]* ) break;;
            * ) echo "Please answer with [y]es or [n]o.";;
        esac
    done
}

show_config () {
    echo "Generated configuration:"
    cat ${CONFIGFILE}
}

check_dependencies () {
    TOOLS=(
        zip
        node
        npm
        serverless
        aws
        curl
        jq
    )
    for i in "${TOOLS[@]}"
    do
        which ${i} >/dev/null 2>&1 || { echo >&2 "I require ${i} but it's not installed."; exit 1; }
    done
}

# check if all configuration files that are needed for deployment exist.
check_configurationfiles () {
    # TODO: implement
    sleep 1
}

prepare_deploy () {
    echo "Preparing deployment"

    echo "  -> Fixing access permissions"
    chmod -R 777 src/functions
    chmod -R 777 config
    chmod -R 777 src/scripts
    chmod +r config/config.json

    echo "  -> Installing dependencies"
    cd src/functions
    npm install
    cd ../..

    check_configurationfiles
}

deploy () {
    echo "Deploying..."

    echo "  -> Batch + ..."
    if [[ "$UPDATE" = true ]] ; then
        echo "Updating Stack..."
        ACTION="update-stack"
        ACTIONWAIT="stack-update-complete"
    else
        echo "Creating Stack..."
        ACTION="create-stack"
        ACTIONWAIT="stack-create-complete"
    fi

    set +e
    aws --region "${REGION}" --profile "${PROFILE}" cloudformation "${ACTION}" --stack-name "${STACKNAME}" \
    --template-body file://cloudformation/svcomp.json --capabilities CAPABILITY_NAMED_IAM \
    --output text --parameters \
    ParameterKey=S3BucketName,ParameterValue=${BUCKETNAME} \
    ParameterKey=RoleECS,ParameterValue=${IAMROLEECS} \
    ParameterKey=ECSInstanceRoleName,ParameterValue=${ECSINSTANCEROLE} \
    ParameterKey=ComputeEnvironmentName,ParameterValue=${COMPUTEENVIRONMENTNAME} \
    ParameterKey=ContainerImage,ParameterValue=${CONTAINERIMAGE} \
    ParameterKey=JobQueueName,ParameterValue=${JOBQUEUENAME} \
    ParameterKey=JobDefinitionName,ParameterValue=${JOBDEFINITIONNAME} \
    ParameterKey=InstanceType,ParameterValue=${INSTANCETYPE} \
    ParameterKey=MaxVCPUs,ParameterValue=${MAXVCPUS} \
    ParameterKey=NumNodes,ParameterValue=${NUMNODES} \
    ParameterKey=VCPUs,ParameterValue=1 \
    ParameterKey=MinMemory,ParameterValue=${MINMEMORY}
    ERROR=$?
    set -e

    echo "  -> Serverless"
    serverless --aws-profile "$PROFILE" --region "$REGION" deploy

    if [[ $ERROR -eq 0 ]] ; then
        echo "  -> ... waiting for cloudformation ..."
        aws --region "${REGION}" --profile "${PROFILE}" cloudformation wait  "${ACTIONWAIT}" --stack-name "${STACKNAME}"
    else
        echo "There was an error when updating/creating Cloudformation Stack"
    fi
}

write_config () {
    echo "Creating config files"

    echo "  -> Creating config.json"
    JSON_STRING=$( jq -n \
                      --arg bn "$BUCKETNAME" \
                      --arg is "$INTERNALSECRET" \
                      --arg ep "$HTTPEndpoint" \
                      --arg rg "$REGION" \
                      --arg es "$IAMROLEECS" \
                      --arg er "$ECSINSTANCEROLE" \
                      --arg bq "$JOBQUEUENAME" \
                      --arg bj "$JOBDEFINITIONNAME" \
                      --arg pf "$PROFILE" \
                      --arg sn "$STACKNAME" \
                      --arg ce "$COMPUTEENVIRONMENTNAME" \
                      --arg st "$STACKNAME" \
                      --arg ci "$CONTAINERIMAGE" \
                      --arg nn "$NUMNODES" \
                      --arg mc "$MAXVCPUS" \
                      --arg mm "$MINMEMORY" \
                      --arg vc "$VCPUSJOB" \
                      --arg it "$INSTANCETYPE" \
                      '{
                        "Stack": $st,
                        "HTTPEndpoint": $ep,
                        "MainBucketName": $bn,
                        "InternalSecret": $is,
                        "Region": $rg,
                        "Profile": $pf,
                        "ECSIAMRole": $es,
                        "ECSInstanceRole": $er,
                        "BatchComputeEnvironment": $ce,
                        "BatchQueue": $bq,
                        "BatchJobDef": $bj,
                        "StackName": $sn,
                        "returnInputOnRequest": true,
                        "ContainerImage": $ci,
                        "NumNodes": $nn,
                        "MaxVCPUS": $mc,
                        "MinMemory": $mm,
                        "InstanceType": $it,
                        "VCPUSPerNode": $vc
                      }' )

    echo "$JSON_STRING" > config/config.tmp.json
    mv config/config.tmp.json "${CONFIGFILE}"

    echo "  -> Creating client config file…"
    JSON_STRING=$( jq -n \
                      --arg s "$TOKEN" \
                      --arg e "$HTTPEndpoint" \
                      '[
                        {
                          "Endpoint": $e,
                          "UserToken": $s,
                          "UserName": "ALL_USERS"
                        }
                      ]
    '                 )
    echo "$JSON_STRING" > all_users.client.config

    echo "  -> Creating authorized users file"
    JSON_STRING=$( jq -n \
                      --arg s "$TOKEN" \
                      '[
                        {
                          "name": "ALL_USERS",
                          "token": $s
                        }
                      ]
    '                 )
    echo "$JSON_STRING" > config/authorized_users
}

remove_bucket () {
    aws --profile "$PROFILE" --region "$REGION" s3 rb s3://"${BUCKETNAME}" --force
}

remove_resources () {
    echo "Waiting for resources to be deleted…"
    aws --region "${REGION}" --profile "${PROFILE}" cloudformation delete-stack --stack-name "${STACKNAME}"
    echo "  -> Waiting for serverless"
    serverless --aws-profile "$PROFILE" --region "$REGION" remove
    echo "  -> Waiting for cloudformation"
    aws --region "${REGION}" --profile "${PROFILE}" cloudformation wait stack-delete-complete --stack-name "$STACKNAME"

    if [[ "$FORCEDELETE" == true ]] ; then
        remove_bucket
    else
        echo "The main data S3 Bucket for this installation (${BUCKETNAME}) was NOT removed."
        echo "Do you wish to delete the bucket and all files stored inside it?"
        while true; do
            read -p "Delete Bucket? [yn]: " yn
            case $yn in
                [Yy]* ) remove_bucket; break;;
                [Nn]* ) break;;
                * ) echo "Please answer with [y]es or [n]o.";;
            esac
        done
    fi

    rm "${CONFIGFILE}"
}

upload_sleep_script () {
   aws --profile "${PROFILE}" s3 cp scripts/sleep-script.sh "s3://${BUCKETNAME}/sleep-script.sh"
}

print_help () {
  echo "SV-COMP Parallel Track on AWS Cloud Deployment Tool"
  echo "https://gitlab.com/sosy-lab/sv-comp/aws-cloud"
  echo ""
  echo "./cloudformation.sh"
  echo "    [(-p|--profile) <aws-profile-name>]"
  echo "       Sets the profile name for the AWS CLI. Default: default"
  echo "       You can configure this with 'aws configure'"
  echo "    [(-c|--config) <path-to-config-file>]"
  echo "       Sets the config file. Default: config/config.json"
  echo "    [(-r|--region) <aws-region>]"
  echo "      Sets the aws region to <aws-region>. Default: eu-west-1 (Ireland)"
  echo "    [(-d|--delete)]"
  echo "      Removes all resources from the AWS account (according to config file)"
  echo "      Asks for confirmation interactively. "
  echo "    [--forcedelete]"
  echo "      Same as --delete, but does not ask confirmation interactively."
  echo "      Caution: Automatically removes the S3 Bucket!"
  echo "    [test --(portfolio | hello-world | multi-node | integration-predicate-overflow | predicate | clean)]"
  echo "      -> portfolio: (3 Tasks)"
  echo "      -> hello-world: Tests execution of AWS Batch Jobs (1 Task)"
  echo "      -> multi-node: Tests inter-node communication (3 Tasks)"
  echo "      -> integration-predicate-overflow: (50 Tasks)"
  echo "      -> predicate: (1530 Tasks)"
  echo "      -> clean: Removes all assets that were downloaded/created during tests."
}