#!/bin/bash

# This file is part of AWS Cloud,
# a framework to perform massively parallel verification using AWS:
# https://gitlab.com/sosy-lab/sv-comp/aws-cloud
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

echo "Starting sleep script"
echo "Sleepig 10s"
sleep 10
echo "Done!"

function sleep20() {
  echo "Starting sleep function"
  echo "Sleepig 20s"
  sleep 20
  echo "Done!"
}