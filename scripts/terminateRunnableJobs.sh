#! /bin/bash

# This file is part of AWS Cloud,
# a framework to perform massively parallel verification using AWS:
# https://gitlab.com/sosy-lab/sv-comp/aws-cloud
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

CONFIGFILE="../config/config.json"
while [[ $# -gt 0 ]] ; do
  key="$1"
  case $key in
    -c|--config)
      CONFIGFILE="$2"
      shift
      ;;
    *)
      echo "Invalid param received: $key. Aborting script." >&2
      exit 1
      ;;
  esac
  shift
done

if [[ -f "$CONFIGFILE" ]]; then
    REGION=$(jq -r '.Region' "$CONFIGFILE")
    PROFILE=$(jq -r '.Profile' "$CONFIGFILE")
    JOBQUEUENAME=$(jq -r '.BatchQueue' "$CONFIGFILE")

    for i in $(aws --profile "${PROFILE}" --region "${REGION}" batch list-jobs --job-queue "${JOBQUEUENAME}" --job-status runnable --output text --query jobSummaryList[*].[jobId])
    do
      aws --profile "${PROFILE}" --region "${REGION}" batch terminate-job --job-id $i --reason "User"
      echo "Deleted $i"
    done
else
    echo "Cannot find config file"
    exit 1
fi

