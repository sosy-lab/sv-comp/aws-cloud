#!/usr/bin/env bash

# This file is part of AWS Cloud,
# a framework to perform massively parallel verification using AWS:
# https://gitlab.com/sosy-lab/sv-comp/aws-cloud
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

mkdir -p config
mkdir -p ~/.config/sv-comp-aws/
docker run -v "${DIR}":/home/aws \
           -v ~/.aws:/root/.aws \
           -v ~/.config/sv-comp-aws:/root/.config/sv-comp-aws \
           -ti registry.gitlab.com/sosy-lab/sv-comp/aws-cloud/cli "$@"