# This file is part of AWS Cloud,
# a framework to perform massively parallel verification using AWS:
# https://gitlab.com/sosy-lab/sv-comp/aws-cloud
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive
RUN ln -fs /usr/share/zoneinfo/Europe/Berlin /etc/localtime
RUN apt-get update && apt-get upgrade -y && apt-get install -y \
       python3-pip \
       wget \
       git \
       zip \
       unzip \
       npm \
       curl \
       jq \
       build-essential

RUN curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh
RUN bash nodesource_setup.sh
RUN apt-get install -y nodejs

RUN npm install -g serverless
RUN pip3 install awscli --upgrade

RUN mkdir /home/aws
RUN cd /home/aws

WORKDIR "/home/aws"
ENTRYPOINT ["bash", "cloudformation.sh"]
