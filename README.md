<!--
This file is part of AWS Cloud,
a framework to perform massively parallel verification using AWS:
https://gitlab.com/sosy-lab/sv-comp/aws-cloud

SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# Wall-Time Track of SV-COMP, Executed on AWS Cloud

## Track Description

The SV-COMP is a competition for software verification to evaluate and compare
verification tools against each other. This is done in a standardized environment,
to ensure equal conditions for all participating tools.


To further showcase the different strengths of the verifiers, community members
are able to propose new demonstation categories for each upcoming SV-COMP,
which can then additionally extend the SV-COMP in its main categories.


One of such proposals is a *walltime*-track to evaluate the speedup of parallel verifaction.
That is, given a verification task, a verifier can use a large number of processes
to solve the verification task, distributing the verification work such that the wall time
becomes smaller compared to measuring CPU time. The consumed CPU time does not matter for the ranking
in this track. The number of processes that run in parallel can in principle scale to any number, and is
for the time being set to 20. The processes can communicate over SSH, for example by using the MPI standard.


## License and Copyright

AWS Cloud is licensed under the [Apache 2.0 License](https://www.apache.org/licenses/LICENSE-2.0),
copyright [Dirk Beyer](https://www.sosy-lab.org/people/beyer/).
Exceptions are third-party code that are explicitely marked as such and available under
several other free licenses
(cf. folder [`LICENSES`](LICENSES)).

## Resources

The resources for training and competition execution are sponsored by Amazon Web Services.
We use [Benchexec](https://github.com/sosy-lab/benchexec) to execute the verification runs
in containers on the AWS cloud. The core feature used from AWS is in particular
the batch-processing service `AWS Batch`.

Each task will have access to 20 `c5.2xlarge` instances, 
with each `c5.2xlarge` instance containing 8 vCPUs and 16 GiB memory.

## Participation

Please send an e-mail to [Dirk Beyer](https://www.sosy-lab.org/people/beyer/) if you are interested in participating.

## Deadlines

April 30:  Register intent to participate via e-mail to Dirk Beyer<br/>
May   31:  Make tool available via the usual SV-COMP GitLab repository<br/>
June  30:  Submission of final version of verifiers

## Acknowledgement

Thanks to Mike Whalen for arranging with AWS
- to fully sponsor the resources for the wall-time track and
- to sponsor the development of the infrastructure, BenchExec extension, and example application.


# Requirements

## AWS account

To run this demo track on your own, we recommend that you create a new AWS account
for security and maintainability reasons, and to ensure the billing of your account.

In order to create a new AWS account, simply head to [aws.amazon.com](https://aws.amazon.com)
and follow the instructions provided there.
When creating your account, ideally do that with the name of your verifier and `sv-comp` in your name.
Please note that during the process you will eventually be asked for a cell phone number, a credit card, and an address.

Once the account is created, please send an email with your `account ID` to Dirk Beyer, such that he can
arrange the credits to be accordingly assigned to your account.
The account ID can be found on the AWS website by 1.) clicking on your account name in the top right corner,
and 2.) choosing `My Account` afterwards.
The account ID is the top-most entry in the `Account Settings` section.

We recommend that you send the mail as soon as possible once your account has been created, to speed up
the process of applying the resource budget for your experiments.

## Installation instructions
This section describes the prerequisites necessary for setting everything up. 

### BenchExec

First, a working installation of [Benchexec](https://github.com/sosy-lab/benchexec) is required.
Please refer to the official repository for further
[installation instructions](https://github.com/sosy-lab/benchexec/blob/master/doc/INSTALL.md).

### AWS CLI

Next, a fully working AWS CLI setup is required. An official installation guide is
provided at https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html.
Once installed, please make sure to configure it using the command `aws configure` on your console. A detailed guide for
the setup can be found at https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html.

Upon finishing the configuration, the files `~/.aws/config` and `~/.aws/credentials` should be available
that contain something similar to the following input:
    
    $ cat ~/.aws/config
    [default]
    region = eu-central-1
    output = json

    $ cat ~/.aws/credentials
    [default]
    aws_access_key_id = <YOUR_ACCESS_ID>
    aws_secret_access_key = <YOUR_SECRET_ACCESS_KEY>



### Docker (recommended)

We provide two ways for the installation process: one is the manual installation of dependencies, and 
the other the installation by means of a Docker Image. We recommend to use the latter as it allows
the setup without the manual installation of dependencies on your local machine.

When choosen the installation using Docker, always use the `bash aws-svcomp.sh` option (over `bash cloudformation`) in the following sections.

The Docker Image used for the setup is based on the [Dockerfile](Dockerfile) in this main directory.
You can alternatively build your own image based on it and update the according image tag in the `aws-svcomp.sh` file. 

### Manual

Skip this step if you decided to use Docker for the setup and continue with the [installation section](###installation) from below. 
Otherwise, you need to make sure to have the following tools on your system installed:
- zip
- node
- npm
- serverless
- jq

An example on how to install `node` can be found at https://nodejs.org/en/download/package-manager/

Ubuntu users can alternatively take a look at the binary distributions from NodeSource: https://github.com/nodesource/distributions/blob/master/README.md

Once you have node and npm running, install the `serverless` framework as follows:

    npm install serverless -g

The other packages can be installed using the default package manager from your Linux distribution.

## Automatic installation with Cloudformation

### Installation: 

    bash aws-svcomp.sh
    bash cloudformation.sh   # (manual installation)

Parameters (optional):
    
    --region <AWS-REGION>   (default: eu-west-1)
    --profile <AWS-PROFILE> (default: default)

The tool chooses default parameters. For the first installation we suggest that you keep the default values
and update the configuration only once you have verified that everything works at a later stage (c.f. [how to update](#Update)).

### Removal:

    bash aws-svcomp.sh --delete 
    bash cloudformation.sh --delete # (manual installation)

This will remove the Cloudformation stacks. The script will ask you if you want to retain the S3 Bucket and its content.
You can in general safely remove it, however, after that the content cannot be restored anymore and the name of the bucket
will be made available for others to register.

### Update
You can make changes to the config file (`config/config.json`) and rerun `bash aws-svcomp.sh` (`bash cloudformation.sh`) to update the stack. 
The following values can be changed:
    
    - MainBucketName": "xxx",
    - Profile": "default",
    - ECSIAMRole": "xxx",
    - ECSInstanceRole": "xxx",
    - BatchComputeEnvironment": "xxx",
    - BatchQueue": "xxx",
    - BatchJobDef": "xxx",
    - returnInputOnRequest": true,
    - ContainerImage": "registry.gitlab.com/sosy-lab/sv-comp/aws-cloud",
    - NumNodes": "20",
    - MaxVCPUS": "256",
    - MinMemory": "2048",
    - InstanceType": "c5.2xlarge",
    - VCPUSPerNode": "1"

## Docker container for the benchmark runs
AWS batch requires the user to provide a docker container. This is taken for the environment in which the verifier tool 
is executed (this is a different Docker Image to the one described in an earlier section and not to be confused with it).
We recommend to use our predefined registry that was built from the [image here](./docker/Dockerfile).
It is available for public use and is available at (registry.gitlab.com/sosy-lab/sv-comp/aws-cloud:latest).

You can alternatively also configure your own container, however, in this case you should base it on the Dockerfile
that we provide (see above). This is in order to not break the functionality when running your verifier on AWS.
You can build your own container using the following commands:

    cd docker
    sudo docker build -t <namespace>/<repository-name> .
    sudo docker push <namespace>/<repository-name>

The container can be pushed to a Docker registry of your choosing, such as e.g. Docker Hub.
AWS also offers a service to host your docker container. It is called *Elastic Container Registry* (ECR), which
we have at times used ourselves for this project.
More information about the service can be found at (https://aws.amazon.com/ecr).

## Optional: run a script that tests your config
The setup is now complete at this point. You can either test your configuration next by executing a test script
provided in this repository, or instead skip the testing and directly proceed with the next section [execution](#execution).

To test your setup, run `bash aws-svcomp.sh test` (`bash cloudformation.sh test`) from your command line.<br\>
Available parameters for the script:
- `--hello-world`: MPI Hello World Application
- `--multi-node`: Portfolio analysis on multiple nodes. This uses the tool [CPAchecker](https://gitlab.com/sosy-lab/software/cpachecker/) in order to verify three example tasks.

Executing the test-script will take approximately 5 minutes.

# Execution

The process for verifying a complete run-collection is started by a python file called
[aws-benchmark.py](https://github.com/sosy-lab/benchexec/tree/master/contrib).
It is stored in [Benchexec](https://github.com/sosy-lab/benchexec) in the directory `contrib`.
The script is currently expected to be executed from within the verifier directory and requires an `--aws`-flag
in order to do the benchmarking using the AWS infrastructure (otherwise, the verifying tasks will be executed on your local machine only).
From within the verifier directory, the script can be invoked as follows: \
`path/to/aws-benchmark.py --aws <benchmark-def>.xml`

[Outdated information] An `--awsConfig` -flag is additionally required together with a path pointing to the config file. The config file is named `<user>.client.config` by default and should be located in your local directory where the AWS repository resides. Below is the command-line that needs to be executed: \
`path/to/aws-benchmark.py --aws --awsConfig path/to/<user>.client.config <benchmark-def>.xml`

Please note that it might take several minutes (up to 5 minutes) for `AWS batch` to start the first processes.

Invoking the python script as described above is only temporary until further testing has been done. After that it is intended to have this functionality integrated into the main application of BenchExec.

If the exeuction of benchexec was successful, a new folder should be available in your verifier directory unter `test/results/`
that contains all of the result files. These can be evaluated afterwards using the `table-generator` feature of Benchexec.
The command-line for that is as follows: \
`table-generator path-to/test/results/<benchmark>.xml.bz2`

This will generate an html-file that can be viewed in the browser of your choice.

# Other

## Scripts (optional)
(scripts/global-ec2-resources.sh): This script will list all EC2 instances in every region.
Use this to validate that all instances have actually terminated so that you will not incur any charges accidentally.

(scripts/terminateRunnableJobs --config <path-to-config-file.json>): This script terminates all Jobs that are listed
in AWS Batch in the state `runnable`. If no config file is passed, it will try to use `../config/config.json` as default.
