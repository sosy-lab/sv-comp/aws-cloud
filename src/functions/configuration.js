// This file is part of AWS Cloud,
// a framework to perform massively parallel verification using AWS:
// https://gitlab.com/sosy-lab/sv-comp/aws-cloud
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

let fs = require('fs');
exports.config = JSON.parse(fs.readFileSync("config/config.json", 'utf8'));