// This file is part of AWS Cloud,
// a framework to perform massively parallel verification using AWS:
// https://gitlab.com/sosy-lab/sv-comp/aws-cloud
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

/**
 *
 * @param user
 * @param command
 * @returns {string}
 */
exports.runAsUser = function (user, command) {
    return 'su -c "' + command + '" ' + user + '\n';
};

/**
 *
 * @param event
 * @param status
 * @param result
 * @returns {{body: string, statusCode}}
 */
exports.send = function(event, status, result) {
    let config = require('./configuration').config;
    result.input = config['returnInputOnRequest'] ? event : "return input has been disabled";
    return {
        statusCode: status,
        body: JSON.stringify(
            result,
            null,
            2
        ),
    };
}

/**
 *
 * @param token
 * @returns {Promise<boolean|*>}
 */
let check_auth = async function(token) {
    console.log("checking authorization")
    let config = require('./configuration').config;
    let amazon = require('./aws').aws;
    console.log(config);
    //let data = await amazon.s3.getObject({Bucket: config['MainBucketName'], Key: 'authorized_users'}).promise();
    let fs = require('fs');
    let data = fs.readFileSync("config/authorized_users", 'utf8');
    //let users = J..SON.parse(data.Body.toString());
    let users = JSON.parse(data);

    console.log("AuthorizedUsers:");
    console.log(users);

    console.log("Searching for token: " + token);
    for (let i = 0; i < users.length; i++) {
        if (users[i].token === token) {
            console.log("User was found in authorized_users list");
            return users[i].name;
        }
    }

    console.log("User was *not* found in authorized_users list");
    return false;
}
exports.check_authorization = check_auth;

/**
 *
 * @param event
 * @returns {Promise<*|boolean|{body: string, statusCode}>}
 */
exports.auth = async function (event) {
    let amazon = require('./aws').aws;
    let result = {};
    let username = await check_auth(event.pathParameters['user'], amazon);
    if (username === false) {
        result.message = "Token not authorized.";
        return exports.send(event, 401, result);
    }
    return username;
};

/**
 *
 * @param event
 * @returns {Promise<*|boolean>}
 */
exports.validateJobId = async function (event) {
    let result = {};
    let jobid = await isJobIdValid(event.pathParameters['id']);
    if (jobid === false) {
        result.message = "Job not found.";
        exports.send(event, 404, result);
    }
    return jobid;
};

/**
 *
 * @param jobid
 * @returns {Promise<boolean|*>}
 */
let isJobIdValid = async function(jobid) {
    let config = require('./configuration').config;
    let amazon = require('./aws').aws;
    let cloud = require('./aws').cloudFunctions;
    let s3Objects = await amazon.s3.listObjectsV2({
        Bucket: config['MainBucketName'],
        Prefix: 'jobs/' + jobid + "/"
    }).promise();

    if (s3Objects === undefined || s3Objects["Contents"] === undefined) {
        return false;
    } else if (s3Objects["Contents"].length > 0) {
        return jobid;
    } else {
        return false;
    }
}
exports.isJobIdValid = isJobIdValid;

/**
 * Deletes all Keys with a specific prefix from S3
 * From: https://stackoverflow.com/questions/20207063/how-can-i-delete-folder-on-s3-with-node-js
 * @param bucket
 * @param dir Prefix
 * @returns {Promise<void>}
 */
exports. emptyS3Directory = async function emptyS3Directory(bucket, dir) {
    let amazon = require('./aws').aws;
    const listParams = {
        Bucket: bucket,
        Prefix: dir
    };

    const listedObjects = await amazon.s3.listObjectsV2(listParams).promise();

    if (listedObjects.Contents.length === 0) return;

    const deleteParams = {
        Bucket: bucket,
        Delete: { Objects: [] }
    };

    listedObjects.Contents.forEach(({ Key }) => {
        deleteParams.Delete.Objects.push({ Key });
    });

    await amazon.s3.deleteObjects(deleteParams).promise();

    if (listedObjects.IsTruncated) await exports.emptyS3Directory(bucket, dir);
}