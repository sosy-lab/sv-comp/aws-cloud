// This file is part of AWS Cloud,
// a framework to perform massively parallel verification using AWS:
// https://gitlab.com/sosy-lab/sv-comp/aws-cloud
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

/**
 *
 * @param jobid
 * @param parameters
 * @returns {string}
 */
exports.createScript = function (jobid, parameters) {
    let configuration = require('./configuration').config;
    let fs = require("fs");

    // Create commands
    let commands = "#!/bin/bash\n" +
        "set -x\n";

    if (parameters.helloWorld) {
        let jobParameters = {};
        let template = fs.readFileSync('src/scripts/helloworld-job.sh', "utf8");
        commands += "\n" +
            "function job0 () {\n" +
                replacePlaceholdersInTemplate(template, jobid, parameters, jobParameters, configuration) +
            "\n}\n";

    } else {
        let template = fs.readFileSync('src/scripts/job.sh', "utf8");

        // runDefinitions is the set of runs to be executed
        for (let i = 0; i < parameters.runDefinitions.length; i++) {
            // jobParameters are the parameters that change for every job
            let jobParameters = {
                resultFolder: parameters.runDefinitions[i]['log_file'].slice(0, -4),
                logfileName: parameters.runDefinitions[i]['log_file'].slice(0, -4),
                cmdLine: parameters.runDefinitions[i].cmdline
            }

            commands += "\n" +
                "function job" + i + " () {\n" +
                    replacePlaceholdersInTemplate(template, jobid, parameters, jobParameters, configuration) +
                "\n}\n";
        }
    }

    return commands;
};

/**
 *
 * @param template
 * @param jobid
 * @param parameters
 * @param jobParameters
 * @param configuration
 * @returns {*}
 */
function replacePlaceholdersInTemplate(template, jobid, parameters, jobParameters, configuration) {
    // Remove all lines starting with '#!'
    template = template.replace(/^#!(.*)$/mg, '');

    template = template.replaceAllOccurrences("!!MainBucketName!!", configuration["MainBucketName"]);
    template = template.replaceAllOccurrences("!!JobId!!", jobid);
    template = template.replaceAllOccurrences("!!TasksArchiveS3Key!!", parameters.tasksArchiveS3Key);
    template = template.replaceAllOccurrences("!!TasksArchiveUrl!!", parameters.tasksArchiveUrl);
    template = template.replaceAllOccurrences("!!VerifierArchiveS3Key!!", parameters.verifierArchiveS3Key);
    template = template.replaceAllOccurrences("!!VerifierArchiveUrl!!", parameters.verifierArchiveUrl);
    template = template.replaceAllOccurrences("!!WorkingDir!!", parameters.workingDir);

    template = template.replaceAllOccurrences("!!ResultFolder!!", jobParameters.resultFolder);
    template = template.replaceAllOccurrences("!!LogfileName!!", jobParameters.logfileName);
    template = template.replaceAllOccurrences("!!CMDLine!!", jobParameters.cmdLine);

    return template;
}

String.prototype.replaceAllOccurrences = function(str1, str2, ignore)
{
    return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
};