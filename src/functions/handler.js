// This file is part of AWS Cloud,
// a framework to perform massively parallel verification using AWS:
// https://gitlab.com/sosy-lab/sv-comp/aws-cloud
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

'use strict';

// TODO: Add a delete endpoint to remove data from S3 and EFS bases on JobId
// TODO: Add support for large S3 responses (pagination)
// TODO: properly validate user input
// TODO: Create swagger file for API documentation
// TODO: Add proper inline documentation to function calls
// TODO: jobs is currently not used

// General Configuration
const configuration = require('./configuration').config;
const amazon = require('./aws').aws;
const cloud = require('./aws').cloudFunctions;
const f = require('./functions');

/**
 *
 * @param event
 * @returns {Promise<{body: string, statusCode: *}>}
 */
module.exports.create = async event => {
    const uuidv4 = require('uuid/v4');
    
    let username = await f.auth(event);
    let info = {
        requestId: uuidv4()
    };

    await cloud.storeObject(
        configuration["MainBucketName"],
        'jobs/' + info.requestId + '/info.json',
        info
    );

    return f.send(event, 200, {
        requestId: info.requestId
    });
};

/**
 *
 * @param event
 * @returns {Promise<{body: string, statusCode: *}>}
 */
module.exports.generateS3UploadUrl = async event => {
    let username = await f.auth(event);
    let jobid = await f.validateJobId(event);
        
    let key = "jobs/" + jobid + "/uploads/" + event['queryStringParameters']['file'];

    return f.send(event, 200, {
        uploadUrl: cloud.getSignedUrlPutObject({
            'Bucket': configuration["MainBucketName"],
            'Key': key,
            'Expires': 600,
            'ACL': 'public-read'
        }),
        S3Key: key,
        S3Bucket: configuration["MainBucketName"],
        publicURL: "http://" + configuration["MainBucketName"] + ".s3." + configuration["Region"] + ".amazonaws.com/" + key,
        message: "",        
    });
};

/**
 *
 * @param event
 * @returns {Promise<{body: string, statusCode: *}>}
 */
module.exports.launchBatch = async event => {
    let username = await f.auth(event);
    let jobid = await f.validateJobId(event);

    // use hello world example if tasks are not set
    let helloWorldExample = event['queryStringParameters']['tasks'] === null
                             || event['queryStringParameters']['tasks'] === undefined
                             || event['queryStringParameters']['tasks'] === "";

    let commands = {};
    if (!helloWorldExample) {
        let commandsLocation = event['queryStringParameters']['commandsS3'];
        commands = await cloud.retrieveObject(
            configuration['MainBucketName'],
            commandsLocation
        );
    }

    let parameters = {
        helloWorld: helloWorldExample,
        verifierArchiveUrl: event['queryStringParameters']['verifier'],
        verifierArchiveS3Key: event['queryStringParameters']['verifierS3'],
        tasksArchiveUrl: helloWorldExample ? undefined : event['queryStringParameters']['tasks'],
        tasksArchiveS3Key: helloWorldExample ? undefined : event['queryStringParameters']['tasksS3'],
        runDefinitions: helloWorldExample ? [undefined] : commands['runDefinitions'],
        limitsAndNumRuns: helloWorldExample ? undefined : commands['limitsAndNumRuns'],
        requirements: helloWorldExample ? undefined : commands['requirements'],
        resultFilePatterns: helloWorldExample ? undefined : commands['resultFilePatterns'],
        workingDir: helloWorldExample ? undefined : commands['workingDir']
    };

    // Cannot pass large json objects to submitJobs function -> save in S3, pass location
    await cloud.storeObject(
        configuration["MainBucketName"],
        'jobs/' + jobid + '/parameters.json',
        parameters
    );

    let scriptGenerator = require('./scriptGenerator');
    let scriptContent = scriptGenerator.createScript(jobid, parameters);

    await cloud.storeObject(
        configuration["MainBucketName"],
        'jobs/' + jobid + '/jobs.sh',
        scriptContent,
        true
    )

    let result = {};
    result.bashScript = "s3://" + configuration["MainBucketName"] + '/jobs/' + jobid + '/jobs.sh';
    result.batch = await cloud.startBatch(
        {
            Bucket: configuration["MainBucketName"],
            Key: 'jobs/' + jobid + '/parameters.json',
        },
        jobid,
        result.bashScript
    );

    return f.send(event, 200, result);
};

/**
 *
 * @param event
 * @returns {Promise<{body: string, statusCode: *}>}
 */
module.exports.progressBatch = async event => {    
    let username = await f.auth(event);
    let jobid = await f.validateJobId(event);

    try {
        let ids = await cloud.retrieveObject(
            configuration["MainBucketName"],
            'jobs/' + jobid + '/batchJobs.json'
        );

        // Create array consisting only of Batch JobIDs
        ids = ids.map(function (job) {
            return job.jobId;
        });
        let totalNumberOfJobs = ids.length;
        let completed = 0;
        // Describe Jobs in Batches of 100 JobIDs (maximum set by AWS)
        while (ids.length > 0) {
            let jobIds = ids.splice(0, 100);

            let params = {jobs: jobIds};
            // describeJobs throws an exception if the JobID cannot be found. This happens if the jobs are not yet
            // submitted (Jobs have to be submitted sequentially which can take minutes to finish). The submitBatch
            // function works asynchronously in its own lambda function, which is why we cannot (easily) query the
            // state.
            let batchOverview = await amazon.batch.describeJobs(params).promise();

            for (let i = 0; i < batchOverview.jobs.length; i++) {
                if (batchOverview.jobs[i].status === 'SUCCEEDED' || batchOverview.jobs[i].status === 'FAILED') {
                    completed++
                }
            }

        }

        return f.send(event, 200, {
            submittingJobs: false,
            totalNumberOfJobsCompleted: completed,
            totalNumberOfJobs: totalNumberOfJobs,
            completed: completed === totalNumberOfJobs,
        });
    } catch (e) {
        return f.send(event, 200, {
            submittingJobs: true,
            totalNumberOfJobsCompleted: undefined,
            totalNumberOfJobs: undefined,
            completed: false,
        });
    }
};

/**
 *
 * @param event
 * @returns {Promise<{body: string, statusCode: *}>}
 */
module.exports.results = async event => {
    let username = await f.auth(event);
    let jobid = await f.validateJobId(event);

    let allKeys = await cloud.getAllKeys(configuration['MainBucketName'], 'jobs/' + jobid + "/results/");

    let urls = [];
    let location = await cloud.bucketLocation(configuration['MainBucketName']);
    for (let i = 0; i < allKeys.length; i++) {
        urls.push(
            "https://" + configuration["MainBucketName"] + ".s3." + location.LocationConstraint + ".amazonaws.com/" + allKeys[i]
        );
    }
    
    let result = {
        bucketName: configuration["MainBucketName"],
        urls: urls,
        message: JSON.stringify("")
    };

    return f.send(event, 200, result);
};

/**
 *
 * @param event
 * @returns {Promise<{body: string, statusCode: *}>}
 */
module.exports.cancel = async event => {
    console.log("Starting cancel function");
    let result = {};

    let username = await f.auth(event);

    // Verify JobId
    let jobid = await f.isJobIdValid(event.pathParameters['id']);
    if (jobid === false) {
        result.message = "Job not found.";
        return f.send(event, 404, result);
    }

    let batchJobs = await cloud.retrieveObject(
        configuration['MainBucketName'],
        'jobs/' + jobid + '/batchJobs.json'
    );

    let ids = batchJobs.map(function (job) {
        return job.jobId;
    });
    result.ids = ids;

    await amazon.lambda
        .invoke({
            FunctionName: 'sv-comp-aws-dev-cancelJobs',
            InvocationType: 'Event',
            Payload: JSON.stringify({
                ids: ids
            }),
        }).promise();

    return f.send(event, 200, result);
};

/**
 *
 * @param event
 * @returns {Promise<{body: string, statusCode: *}>}
 */
module.exports.clean = async event => {
    console.log("Starting clean function");
    let result = {};

    let username = await f.auth(event);

    // Verify JobId
    let jobid = await f.isJobIdValid(event.pathParameters['id']);
    if (jobid === false) {
        result.message = "Job not found.";
        return f.send(event, 404, result);
    }

    await f.emptyS3Directory(
        configuration['MainBucketName'],
        "jobs/" + jobid
    )

    result.message = "OK";
    return f.send(event, 200, result);
};

/**
 *
 * @param event
 * @returns {Promise<{body: string, statusCode: *}>}
 */
module.exports.jobs = async event => {
    let username = await f.auth(event);

    return f.send(event, 200, {
        ids: await cloud.commonPrefixes(
            configuration['MainBucketName'],
            "jobs/",
            "/"
        )
    });
};

/**
 *
 * @param event
 * @returns {Promise<void>}
 */
module.exports.submitJobs = async event => {
    let config = require('./configuration').config;
    let parameters = await cloud.retrieveObject(
        event['parametersLocation']['Bucket'],
        event['parametersLocation']['Key']
    );

    let params = event['params'];
    let jobid = event['jobid'];
    let scriptLocation = event['scriptLocation'];

    let result = [];
    for (let i = 0; i < parameters.runDefinitions.length; i++) {
        params.jobName = jobid + "-" + i;
        params.nodeOverrides.nodePropertyOverrides[0].containerOverrides.environment[0].value = "job" + i;

        let batchResult = await amazon.batch.submitJob(params).promise();

        result.push({
            jobName: batchResult.jobName,
            jobId: batchResult.jobId,
            bashFunction: "job" + i,
            bashScript: scriptLocation
        });

        console.log("Submitted " + result.length + " of " + parameters.runDefinitions.length);
    }

    let uploadStatus = await cloud.storeObject(
        config['MainBucketName'],
        'jobs/' + jobid + '/batchJobs.json',
        result
    );

    console.log("Wrote to " + config['MainBucketName'] + "/" + 'jobs/' + jobid + '/batchJobs.json' + " file. ETag: " + uploadStatus.ETag);

};

/**
 *
 * @param event
 * @returns {Promise<void>}
 */
module.exports.cancelJobs = async event => {
    let ids = event['ids'];

    for (let i = 0; i < ids.length; i++) {
        console.log("Canceling: " + ids[i]);
        await amazon.batch.terminateJob({jobId: ids[i], reason: "user"}).promise();
    }
};
