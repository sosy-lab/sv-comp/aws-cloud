// This file is part of AWS Cloud,
// a framework to perform massively parallel verification using AWS:
// https://gitlab.com/sosy-lab/sv-comp/aws-cloud
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

let configuration = require('./configuration').config;
let AWS = require( 'aws-sdk' );
AWS.config.update({region: configuration['Region']});
let amazon = {
    s3: new AWS.S3({apiVersion: '2006-03-01', signatureVersion: 'v4'}),
    batch: new AWS.Batch({apiVersion: '2016-08-10'}),
    lambda: new AWS.Lambda()
}
exports.aws = amazon;

let cloudFunctions= {
    /**
     * Retrieves an object from S3
     * Will return the JSON-parsed object if literal is not set or false
     * @param bucket
     * @param key
     * @param literal (optional)
     * @returns {Promise<any>}
     */
    retrieveObject: async function (bucket, key, literal) {
        let data = await amazon.s3.getObject({
                Bucket: bucket,
                Key: key
            }
        ).promise();
        return (literal === true) ? data.Body.toString() : JSON.parse(data.Body.toString());
    },
    /**
     * Stores an object or literal to S3
     * @param bucket S3 Bucket
     * @param key S3 Key
     * @param info NodeJS Object that will be stored as JSON
     * @param literal (optional) Store object without calling JSON.stringify
     * @returns {Promise<S3.PutObjectOutput & {$response: Response<S3.PutObjectOutput, AWSError>}>}
     */
    storeObject: async function (bucket, key, info, literal) {
        return await amazon.s3.putObject({
            Bucket: bucket,
            Key: key,
            Body: (literal === true) ? info : JSON.stringify(info)
        }).promise();
    },
    /**
     * Starts a Batch Job and calls the submitJobs Lambda function
     * @param parametersLocation
     * @param jobid
     * @param scriptLocation
     * @returns {Promise<{}>}
     */
    startBatch: async function(parametersLocation, jobid, scriptLocation) {
        let config = require('./configuration').config;
        let amazon = require('./aws').aws;

        let params = {
            jobDefinition: config['BatchJobDef'],
            jobName: '<replace>',
            jobQueue: config['BatchQueue'],
            //containerOverrides: containerOverrides,
            nodeOverrides: {
                nodePropertyOverrides: [
                    {
                        containerOverrides: {
                            environment: [
                                {
                                    name: 'BASH_SCRIPT_FUNCTION',
                                    value: '<replace>'
                                },
                                {
                                    name: 'S3_BASH_SCRIPT',
                                    value: scriptLocation
                                }
                            ]
                        },
                        targetNodes: "0:"
                    }
                ]
            },
            timeout: {
                attemptDurationSeconds: '1800' // 30 minutes
            }
        };

        // Call Lambda Function submitJobs
        await amazon.lambda
            .invoke({
                FunctionName: 'sv-comp-aws-dev-submitJobs',
                InvocationType: 'Event',
                Payload: JSON.stringify({
                    params: params,
                    parametersLocation: parametersLocation,
                    jobid: jobid,
                    scriptLocation: scriptLocation
                }),
            }).promise();

        console.log("Started async submit jobs lambda function with parametersLocation=" + JSON.stringify(parametersLocation));

        return {};
    },
    /**
     * Returns all keys in a S3 bucket with a specific prefix
     * @param bucketName
     * @param prefix
     * @returns {Promise<[]>}
     */
    getAllKeys: async function(bucketName, prefix){
        const amazon = require('./aws').aws;
        let keys = [];
        let continuationToken = undefined;

        let truncated = false;
        do {
            let s3Objects = await amazon.s3.listObjectsV2({
                Bucket: bucketName,
                Prefix: prefix,
                ContinuationToken: continuationToken
            }).promise();
            console.log(s3Objects);

            for (let i = 0; i < s3Objects.Contents.length; i++) {
                keys.push(s3Objects.Contents[i].Key);
            }

            truncated = s3Objects.IsTruncated;
            continuationToken = s3Objects.NextContinuationToken;
        } while (truncated)

        console.log("All Keys:" + keys);

        return keys;
    },
    /**
     * Returns the signed S3 Url to upload an asset
     * @param params
     * @returns {string}
     */
    getSignedUrlPutObject: function (params) {
        return amazon.s3.getSignedUrl('putObject', params);
    },
    /**
     * Returns the location (e.g. eu-west-1) of an S3 bucket
     * @param bucket
     * @returns {Promise<S3.GetBucketLocationOutput & {$response: Response<S3.GetBucketLocationOutput, AWSError>}>}
     */
    bucketLocation: async function (bucket) {
        return await amazon.s3.getBucketLocation({Bucket: bucket}).promise();
    },
    /**
     *
     * @param bucket
     * @param prefix
     * @param delimiter
     * @returns {Promise<string[]>}
     */
    commonPrefixes: async function (bucket, prefix, delimiter) {
        let objs = await amazon.s3.listObjectsV2({
            Bucket: bucket,
            Prefix: prefix,
            Delimiter: delimiter
        }).promise();

        // Iterate over all results and get the substring between prefix and delimiter
        return objs.CommonPrefixes.map(function (commonPrefix) {
            return commonPrefix.Prefix.substr(
                commonPrefix.Prefix.lastIndexOf(prefix) + prefix.length,
                commonPrefix.Prefix.lastIndexOf(delimiter)
            );
        });
    }
}
exports.cloudFunctions = cloudFunctions;