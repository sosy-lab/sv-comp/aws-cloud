#!/usr/bin/env bash

# This file is part of AWS Cloud,
# a framework to perform massively parallel verification using AWS:
# https://gitlab.com/sosy-lab/sv-comp/aws-cloud
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# The content of this file will be copied into a function inside the job.sh that is then run in the container.

# Variables:
MAIN_BUCKET_NAME=!!MainBucketName!!
JOB_ID=!!JobId!!
TASKS_ARCHIVE_S3_KEY=!!TasksArchiveS3Key!!
VERIFIER_ARCHIVE_S3_KEY=!!VerifierArchiveS3Key!!
RESULT_FOLDER=!!ResultFolder!!
LOGFILE_NAME=!!LogfileName!!
WORKING_DIR=!!WorkingDir!!
CMDLINE=!!CMDLine!!

# unused variables
# - VerifierArchiveUrl:   !!VerifierArchiveUrl!!
# - TasksArchiveUrl:      !!TasksArchiveUrl!!

RUN_DIR=$HOME/test-programs
AWS_BATCH_STATUS_CODE_FILE=/tmp/batch-status-code
mkdir -p $RUN_DIR
if [[ ${AWS_BATCH_JOB_MAIN_NODE_INDEX} -eq ${AWS_BATCH_JOB_NODE_INDEX} ]]; then
  aws s3 cp s3://${MAIN_BUCKET_NAME}/${VERIFIER_ARCHIVE_S3_KEY} ${RUN_DIR}/hw.zip
  echo 0 > $AWS_BATCH_STATUS_CODE_FILE
else
  while ssh -o StrictHostKeyChecking=accept-new "$USER@${AWS_BATCH_JOB_MAIN_NODE_PRIVATE_IPV4_ADDRESS}" [[ ! -f "${AWS_BATCH_STATUS_CODE_FILE}" ]]; do
    sleep 3
  done
  scp -r "$USER@${AWS_BATCH_JOB_MAIN_NODE_PRIVATE_IPV4_ADDRESS}:${RUN_DIR}/hw.zip" ${RUN_DIR}/hw.zip
fi
unzip -d $RUN_DIR $RUN_DIR/hw.zip
cd $RUN_DIR && make
cd $HOME

source mpi-run.sh

if [[ ${AWS_BATCH_JOB_MAIN_NODE_INDEX} -eq ${AWS_BATCH_JOB_NODE_INDEX} ]]
    then
        log "executing main MPIRUN workflow"
        su -c "mpiexec -n ${MPI_NP} -hostfile ${HOST_FILE_PATH} $HOME/test-programs/mpi_hello_world" - $USER >> logfile.txt

        log "Done! Writing exit code 0 to $AWS_BATCH_EXIT_CODE_FILE and shutting down supervisord"
        echo "0" > $AWS_BATCH_EXIT_CODE_FILE
        kill  $(cat /tmp/supervisord.pid)

        aws s3 cp logfile.txt s3://${MAIN_BUCKET_NAME}/jobs/${JOB_ID}/results/logfile.txt --acl public-read
fi
